/**
 * Apache License
 * Version 2.0, January 2004
 * http://www.apache.org/licenses/
 * <p>
 * TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 * <p>
 * 1. Definitions.
 * <p>
 * "License" shall mean the terms and conditions for use, reproduction,
 * and distribution as defined by Sections 1 through 9 of this document.
 * <p>
 * "Licensor" shall mean the copyright owner or entity authorized by
 * the copyright owner that is granting the License.
 * <p>
 * "Legal Entity" shall mean the union of the acting entity and all
 * other entities that control, are controlled by, or are under common
 * control with that entity. For the purposes of this definition,
 * "control" means (i) the power, direct or indirect, to cause the
 * direction or management of such entity, whether by contract or
 * otherwise, or (ii) ownership of fifty percent (50%) or more of the
 * outstanding shares, or (iii) beneficial ownership of such entity.
 * <p>
 * "You" (or "Your") shall mean an individual or Legal Entity
 * exercising permissions granted by this License.
 * <p>
 * "Source" form shall mean the preferred form for making modifications,
 * including but not limited to software source code, documentation
 * source, and configuration files.
 * <p>
 * "Object" form shall mean any form resulting from mechanical
 * transformation or translation of a Source form, including but
 * not limited to compiled object code, generated documentation,
 * and conversions to other media types.
 * <p>
 * "Work" shall mean the work of authorship, whether in Source or
 * Object form, made available under the License, as indicated by a
 * copyright notice that is included in or attached to the work
 * (an example is provided in the Appendix below).
 * <p>
 * "Derivative Works" shall mean any work, whether in Source or Object
 * form, that is based on (or derived from) the Work and for which the
 * editorial revisions, annotations, elaborations, or other modifications
 * represent, as a whole, an original work of authorship. For the purposes
 * of this License, Derivative Works shall not include works that remain
 * separable from, or merely link (or bind by name) to the interfaces of,
 * the Work and Derivative Works thereof.
 * <p>
 * "Contribution" shall mean any work of authorship, including
 * the original version of the Work and any modifications or additions
 * to that Work or Derivative Works thereof, that is intentionally
 * submitted to Licensor for inclusion in the Work by the copyright owner
 * or by an individual or Legal Entity authorized to submit on behalf of
 * the copyright owner. For the purposes of this definition, "submitted"
 * means any form of electronic, verbal, or written communication sent
 * to the Licensor or its representatives, including but not limited to
 * communication on electronic mailing lists, source code control systems,
 * and issue tracking systems that are managed by, or on behalf of, the
 * Licensor for the purpose of discussing and improving the Work, but
 * excluding communication that is conspicuously marked or otherwise
 * designated in writing by the copyright owner as "Not a Contribution."
 * <p>
 * "Contributor" shall mean Licensor and any individual or Legal Entity
 * on behalf of whom a Contribution has been received by Licensor and
 * subsequently incorporated within the Work.
 * <p>
 * 2. Grant of Copyright License. Subject to the terms and conditions of
 * this License, each Contributor hereby grants to You a perpetual,
 * worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 * copyright license to reproduce, prepare Derivative Works of,
 * publicly display, publicly perform, sublicense, and distribute the
 * Work and such Derivative Works in Source or Object form.
 * <p>
 * 3. Grant of Patent License. Subject to the terms and conditions of
 * this License, each Contributor hereby grants to You a perpetual,
 * worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 * (except as stated in this section) patent license to make, have made,
 * use, offer to sell, sell, import, and otherwise transfer the Work,
 * where such license applies only to those patent claims licensable
 * by such Contributor that are necessarily infringed by their
 * Contribution(s) alone or by combination of their Contribution(s)
 * with the Work to which such Contribution(s) was submitted. If You
 * institute patent litigation against any entity (including a
 * cross-claim or counterclaim in a lawsuit) alleging that the Work
 * or a Contribution incorporated within the Work constitutes direct
 * or contributory patent infringement, then any patent licenses
 * granted to You under this License for that Work shall terminate
 * as of the date such litigation is filed.
 * <p>
 * 4. Redistribution. You may reproduce and distribute copies of the
 * Work or Derivative Works thereof in any medium, with or without
 * modifications, and in Source or Object form, provided that You
 * meet the following conditions:
 * <p>
 * (a) You must give any other recipients of the Work or
 * Derivative Works a copy of this License; and
 * <p>
 * (b) You must cause any modified files to carry prominent notices
 * stating that You changed the files; and
 * <p>
 * (c) You must retain, in the Source form of any Derivative Works
 * that You distribute, all copyright, patent, trademark, and
 * attribution notices from the Source form of the Work,
 * excluding those notices that do not pertain to any part of
 * the Derivative Works; and
 * <p>
 * (d) If the Work includes a "NOTICE" text file as part of its
 * distribution, then any Derivative Works that You distribute must
 * include a readable copy of the attribution notices contained
 * within such NOTICE file, excluding those notices that do not
 * pertain to any part of the Derivative Works, in at least one
 * of the following places: within a NOTICE text file distributed
 * as part of the Derivative Works; within the Source form or
 * documentation, if provided along with the Derivative Works; or,
 * within a display generated by the Derivative Works, if and
 * wherever such third-party notices normally appear. The contents
 * of the NOTICE file are for informational purposes only and
 * do not modify the License. You may add Your own attribution
 * notices within Derivative Works that You distribute, alongside
 * or as an addendum to the NOTICE text from the Work, provided
 * that such additional attribution notices cannot be construed
 * as modifying the License.
 * <p>
 * You may add Your own copyright statement to Your modifications and
 * may provide additional or different license terms and conditions
 * for use, reproduction, or distribution of Your modifications, or
 * for any such Derivative Works as a whole, provided Your use,
 * reproduction, and distribution of the Work otherwise complies with
 * the conditions stated in this License.
 * <p>
 * 5. Submission of Contributions. Unless You explicitly state otherwise,
 * any Contribution intentionally submitted for inclusion in the Work
 * by You to the Licensor shall be under the terms and conditions of
 * this License, without any additional terms or conditions.
 * Notwithstanding the above, nothing herein shall supersede or modify
 * the terms of any separate license agreement you may have executed
 * with Licensor regarding such Contributions.
 * <p>
 * 6. Trademarks. This License does not grant permission to use the trade
 * names, trademarks, service marks, or product names of the Licensor,
 * except as required for reasonable and customary use in describing the
 * origin of the Work and reproducing the content of the NOTICE file.
 * <p>
 * 7. Disclaimer of Warranty. Unless required by applicable law or
 * agreed to in writing, Licensor provides the Work (and each
 * Contributor provides its Contributions) on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied, including, without limitation, any warranties or conditions
 * of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 * PARTICULAR PURPOSE. You are solely responsible for determining the
 * appropriateness of using or redistributing the Work and assume any
 * risks associated with Your exercise of permissions under this License.
 * <p>
 * 8. Limitation of Liability. In no event and under no legal theory,
 * whether in tort (including negligence), contract, or otherwise,
 * unless required by applicable law (such as deliberate and grossly
 * negligent acts) or agreed to in writing, shall any Contributor be
 * liable to You for damages, including any direct, indirect, special,
 * incidental, or consequential damages of any character arising as a
 * result of this License or out of the use or inability to use the
 * Work (including but not limited to damages for loss of goodwill,
 * work stoppage, computer failure or malfunction, or any and all
 * other commercial damages or losses), even if such Contributor
 * has been advised of the possibility of such damages.
 * <p>
 * 9. Accepting Warranty or Additional Liability. While redistributing
 * the Work or Derivative Works thereof, You may choose to offer,
 * and charge a fee for, acceptance of support, warranty, indemnity,
 * or other liability obligations and/or rights consistent with this
 * License. However, in accepting such obligations, You may act only
 * on Your own behalf and on Your sole responsibility, not on behalf
 * of any other Contributor, and only if You agree to indemnify,
 * defend, and hold each Contributor harmless for any liability
 * incurred by, or claims asserted against, such Contributor by reason
 * of your accepting any such warranty or additional liability.
 * <p>
 * END OF TERMS AND CONDITIONS
 * <p>
 * APPENDIX: How to apply the Apache License to your work.
 * <p>
 * To apply the Apache License to your work, attach the following
 * boilerplate notice, with the fields enclosed by brackets "[]"
 * replaced with your own identifying information. (Don't include
 * the brackets!)  The text should be enclosed in the appropriate
 * comment syntax for the file format. We also recommend that a
 * file or class name and description of purpose be included on the
 * same "printed page" as the copyright notice for easier
 * identification within third-party archives.
 * <p>
 * Copyright 2021 徐浪 1053618636@qq.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mydataharbor.executor;

import lombok.extern.slf4j.Slf4j;
import mydataharbor.*;
import mydataharbor.exception.ResetException;
import mydataharbor.exception.TheEndException;
import mydataharbor.monitor.TaskExecutorMonitor;
import mydataharbor.setting.BaseSettingContext;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @param <T> 原始介质数据
 * @param <P> 平台协议数据
 * @param <R> writer数据
 * @param <S> 配置上下文
 * @auth xulang
 * @Date 2021/4/30
 **/
@Slf4j
public abstract class AbstractDataExecutor<T, P extends IProtocalData, R, S extends BaseSettingContext> extends Thread implements Closeable {

  private IDataPipline<T, P, R, S> dataPipline;

  private S settingContext;

  /**
   * 是否结束
   */
  private volatile boolean run = true;

  /**
   * 是否暂停
   */
  private volatile boolean suspend = false;

  /**
   * 线程是否结束
   */
  private volatile boolean end = true;

  /**
   * 处理计数器
   */
  private volatile AtomicLong writeCount = new AtomicLong();

  private List<IExecutorListener> executorListeners = new CopyOnWriteArrayList<>();

  /**
   * 并行处理线程池
   */
  protected ForkJoinPool forkJoinPool;

  private Map<Object, Boolean> rollbackUnit;

  private TaskExecutorMonitor taskmonitor;

  public AbstractDataExecutor(IDataPipline<T, P, R, S> dataPipline, String threadName) {
    this.dataPipline = dataPipline;
    this.settingContext = dataPipline.settingContext();
    setName(threadName);
  }

  private void safeListenerRun(ISafeRun run) {
    try {
      run.run();
    } catch (Throwable throwable) {
      log.error("通知listener时发生异常", throwable);
    }
  }

  public void addListener(IExecutorListener executorListener) {
    executorListeners.add(executorListener);
  }

  public IDataPipline<T, P, R, S> getDataPipline() {
    return dataPipline;
  }

  /**
   * 数据产生处
   */
  @Override
  public void run() {
    end = false;
    taskmonitor.setEnd(end);
    if (!suspend)
      safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onRun(this, dataPipline)));
    IDataSource<T, S> dataSource = dataPipline.dataSource();
    IProtocalDataConvertor<T, P, S> protocalDataConvertor = dataPipline.protocalDataConvertor();
    IProtocalDataChecker<P, S> checker = dataPipline.checker();
    IDataConvertor<P, R, S> dataConvertor = dataPipline.dataConventer();
    IDataSink<R, S> sink = dataPipline.sink();
    try {
      taskmonitor.setTotal(dataSource.total());
      while (run) {
        while (suspend) {
          if (!run) {
            //允许暂停时被结束
            break;
          }
          taskmonitor.setLastRunTime(System.currentTimeMillis());
          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            log.error("暂停被打断");
          }
        }
        if (!run) {
          break;
        }
        long startTime = System.currentTimeMillis();
        try {
          doRun(dataSource, protocalDataConvertor, checker, dataConvertor, sink);
        } catch (TheEndException e) {
          //数据拉取完毕
          //跳出循环
          break;
        } finally {
          taskmonitor.setLastRunTime(System.currentTimeMillis());
          taskmonitor.useTimeIncrease(System.currentTimeMillis() - startTime);
        }
        if (settingContext.getSleepTime() != 0L) {
          try {
            Thread.sleep(settingContext.getSleepTime());
          } catch (InterruptedException e) {
            log.error("睡眠被打断", e);
          }
        }
      }
      safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onSucccessEnd(this, dataPipline, writeCount.longValue(), run)));
    } catch (Throwable e) {
      log.error("发生未知异常任务线程异常退出", e);
      safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onExceptionEnd(this, dataPipline, e, writeCount.longValue())));
    } finally {
      end = true;
      taskmonitor.setEnd(end);
      if (run)
        safeListenerRun(this::close);
      if (forkJoinPool != null) {
        forkJoinPool.shutdown();
      }
      log.info("{}该线程结束！", getName());
    }

  }

  private void doRun(IDataSource<T, S> dataProvider, IProtocalDataConvertor<T, P, S> protocalDataConvertor, IProtocalDataChecker checker, IDataConvertor<P, R, S> dataConvertor, IDataSink<R, S> writer) throws TheEndException {
    //协议转换通过的数据
    List<P> protocalConventSuccess = Collections.synchronizedList(new ArrayList<>());
    //协议转换失败的数据
    List<ErrorRecord<T, Object>> protocalConventError = Collections.synchronizedList(new ArrayList<>());
    //检查通过的数据
    List<P> checkerSuccess = Collections.synchronizedList(new ArrayList<>());
    //检查失败的数据
    List<ErrorRecord<P, AbstractDataChecker.CheckResult>> checkerError = Collections.synchronizedList(new ArrayList<>());
    //转换通过的数据
    List<R> dataConventSuccess = Collections.synchronizedList(new ArrayList<>());
    //转换失败的数据
    List<ErrorRecord<P, Object>> dataConventError = Collections.synchronizedList(new ArrayList<>());
    //写入成功
    List<R> writeSuccess = Collections.synchronizedList(new ArrayList<>());
    //写入失败
    List<ErrorRecord<R, IDataSink.WriterResult>> writeError = Collections.synchronizedList(new ArrayList<>());
    //进入write流程的原始记录
    List<T> tRecordConventSucces = Collections.synchronizedList(new ArrayList<>());
    Iterable<T> tRecordsIterable = dataPipline.dataSource().poll(settingContext);
    boolean empty = !tRecordsIterable.iterator().hasNext();
    if (empty) {
      return;
    }
    rollbackUnit = new ConcurrentHashMap<>();
    Stream<T> stream = StreamSupport.stream(tRecordsIterable.spliterator(), settingContext.isParallel());
    //开启了并行，并且制定了线程数
    if (settingContext.isParallel() && settingContext.getThreadNum() != 0) {
      if (forkJoinPool == null)
        forkJoinPool = new ForkJoinPool(settingContext.getThreadNum());
      try {
        forkJoinPool.submit(() -> {
          forEach(stream, dataProvider, protocalDataConvertor, checker, dataConvertor, writer, protocalConventSuccess, protocalConventError, checkerSuccess, checkerError, dataConventSuccess, dataConventError, writeSuccess, writeError, tRecordConventSucces);
        }).get();
      } catch (InterruptedException | ExecutionException e) {
        log.error("并行执行任务发生异常！", e);
      }
    } else {
      forEach(stream, dataProvider, protocalDataConvertor, checker, dataConvertor, writer, protocalConventSuccess, protocalConventError, checkerSuccess, checkerError, dataConventSuccess, dataConventError, writeSuccess, writeError, tRecordConventSucces);
    }
    log.info("原始数据源数据：{}", tRecordsIterable);
    if (tRecordsIterable instanceof Collection) {
      taskmonitor.addAndGettRecordCount((long) ((Collection) tRecordsIterable).size());
    } else {
      tRecordsIterable.forEach((record) -> taskmonitor.addAndGettRecordCount(1L));
    }
    //日志记录
    log.info("协议转换通过记录:{}", protocalConventSuccess);
    taskmonitor.addAndGetProtocalConventSuccessCount((long) protocalConventSuccess.size());
    if (!protocalConventError.isEmpty()) {
      log.info("协议转换失败记录:{}", protocalConventError);
      taskmonitor.addAndGetProtocalConventErrorCount((long) protocalConventError.size());
    }
    log.info("检查通过记录:{}", checkerSuccess);
    taskmonitor.addAndGetCheckerSuccessCount((long) checkerSuccess.size());
    if (!checkerError.isEmpty()) {
      log.info("检查失败记录:{}", checkerError);
      taskmonitor.addAndGetCheckerErrorCount((long) checkerError.size());
    }
    log.info("数据转换通过记录:{}", dataConventSuccess);
    taskmonitor.addAndGetDataConventSuccessCount((long) dataConventSuccess.size());
    if (!dataConventError.isEmpty()) {
      log.info("数据转换失败记录:{}", dataConventError);
      taskmonitor.addAndGetDataConventErrorCount((long) dataConventError.size());

    }
    //检查错误列表里是否有reset异常，如果有则放弃此次写入和提交
    if (isContainRestException(protocalConventError, checkerError, dataConventError)) {
      log.error("写入前流程发生reset异常！");
      tRecordsIterable.forEach(record -> rollbackUnit.put(dataProvider.rollbackTransactionUnit(record), true));
      dataProvider.rollback(tRecordsIterable, settingContext);
      return;
    }

    if (tRecordConventSucces.size() == 0) {
      //数据全部转换失败，并且无需回滚
      dataProvider.commit(tRecordsIterable, settingContext);
    } else {
      if ((tRecordsIterable instanceof Collection) && ((Collection<T>) tRecordsIterable).size() != tRecordConventSucces.size()) {
        //无法写入的数据先提交掉
        ArrayList<T> tobeCommit = new ArrayList<>((Collection<? extends T>) tRecordsIterable);
        tobeCommit.removeAll(tRecordConventSucces);
        dataProvider.commit(tobeCommit, settingContext);
      }
      //批量数据写入
      if (settingContext.isBatchWrite()) {
        batchWrite(dataProvider, writer, dataConventSuccess, writeSuccess, writeError, tRecordConventSucces);
      } else if (settingContext.isBatchCommit()) {
        //单条写入，批量提交
        dataProvider.commit(tRecordConventSucces, settingContext);
      }
    }
    log.info("写入成功记录:{}", writeSuccess);
    writeCount.addAndGet(writeSuccess.size());
    taskmonitor.addAndGetWriteSuccessCount((long) writeSuccess.size());
    if (!writeError.isEmpty()) {
      log.info("写入失败记录:{}", writeError);
      taskmonitor.addAndGetWriteErrorCount((long) writeError.size());
    }
  }

  /**
   * 暂停
   */
  public void pause() {
    this.suspend = true;
    taskmonitor.setSuspend(suspend);
    safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onSuspend(this, dataPipline, writeCount.longValue())));
  }

  /**
   * 继续
   */
  public void doContinue() {
    this.suspend = false;
    taskmonitor.setSuspend(suspend);
    safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onContinue(this, dataPipline, writeCount.longValue())));
  }

  protected void forEach(Stream<T> stream, IDataSource<T, S> dataProvider, IProtocalDataConvertor<T, P, S> rotocalDataConvertor, IProtocalDataChecker checker, IDataConvertor<P, R, S> dataConvertor, IDataSink<R, S> writer, List<P> protocalConventSuccess, List<ErrorRecord<T, Object>> protocalConventError, List<P> checkerSuccess, List<ErrorRecord<P, IProtocalDataChecker.CheckResult>> checkerError, List<R> dataConventSuccess, List<ErrorRecord<P, Object>> dataConventError, List<R> writeSuccess, List<ErrorRecord<R, IDataSink.WriterResult>> writeError, List<T> tRecordConventSucces) {
    stream.forEach(tRecord -> {
      Object rollbackTransactionUnit = dataProvider.rollbackTransactionUnit(tRecord);
      if (!rollbackUnit.getOrDefault(rollbackTransactionUnit, false) || settingContext.isContinueOnRollbackOccurContinueInOncePoll())
        doForEach(dataProvider, rotocalDataConvertor, checker, dataConvertor, writer, protocalConventSuccess, protocalConventError, checkerSuccess, checkerError, dataConventSuccess, dataConventError, writeSuccess, writeError, tRecordConventSucces, tRecord);
    });
  }


  /**
   * 处理单条数据
   *
   * @param dataProvider
   * @param protocalDataConvertor
   * @param checker
   * @param dataConvertor
   * @param writer
   * @param protocalConventSuccess
   * @param protocalConventError
   * @param checkerSuccess
   * @param checkerError
   * @param dataConventSuccess
   * @param dataConventError
   * @param writeSuccess
   * @param writeError
   * @param tRecordConventSucces
   * @param tRecord
   * @return
   */
  protected void doForEach(IDataSource<T, S> dataProvider, IProtocalDataConvertor<T, P, S> protocalDataConvertor, IProtocalDataChecker checker, IDataConvertor<P, R, S> dataConvertor, IDataSink<R, S> writer, List<P> protocalConventSuccess, List<ErrorRecord<T, Object>> protocalConventError, List<P> checkerSuccess, List<ErrorRecord<P, IProtocalDataChecker.CheckResult>> checkerError, List<R> dataConventSuccess, List<ErrorRecord<P, Object>> dataConventError, List<R> writeSuccess, List<ErrorRecord<R, IDataSink.WriterResult>> writeError, List<T> tRecordConventSucces, T tRecord) {
    //协议转换
    P protocalData = protocalConvent(protocalDataConvertor, protocalConventSuccess, protocalConventError, tRecord);
    if (protocalData == null) {
      //协议转换失败
      return;
    }
    if (checker != null) {
      //数据检查
      AbstractDataChecker.CheckResult checkResult = protocalDataCheck(checker, checkerSuccess, checkerError, protocalData);
      if (!checkResult.isPass()) {
        //数据检查未通过
        return;
      }
    } else {
      log.info("checker没有配置");
      checkerSuccess.add(protocalData);
    }
    //数据转换
    List<R> records = dataConvent(dataConvertor, dataConventSuccess, dataConventError, protocalData);
    if (records == null) {
      //数据转换失败
      log.error("数据转换失败!");
      return;
    }
    tRecordConventSucces.add(tRecord);
    //单条数据写入
    if (!settingContext.isBatchWrite()) {
      singleRecordWrite(dataProvider, writer, writeSuccess, writeError, tRecord, records);
    }
    return;
  }

  /**
   * 是否存在reset exception
   *
   * @param errorRecordList
   * @return
   */
  private boolean isContainRestException(List<?>... errorRecordList) {
    for (List<?> errorRecords : errorRecordList) {
      for (Object errorRecord : errorRecords) {
        ErrorRecord errorRecord1 = (ErrorRecord) errorRecord;
        if (errorRecord1.getUnknownException() != null && errorRecord1.getUnknownException() instanceof ResetException) {
          return true;
        }
      }
    }
    return false;
  }

  private void batchWrite(IDataSource<T, S> dataSource, IDataSink<R, S> sink, List<R> dataConventSuccess, List<R> writeSuccess, List<ErrorRecord<R, IDataSink.WriterResult>> writeError, List<T> tRecordConventSucces) {
    try {
      IDataSink.WriterResult writeResult = sink.write(dataConventSuccess, settingContext);
      if (writeResult.isSuccess()) {
        writeSuccess.addAll(dataConventSuccess);
      } else {
        List<ErrorRecord<R, IDataSink.WriterResult>> writeErrorRecords = dataConventSuccess.stream().map(record -> {
          return ErrorRecord.<R, IDataSink.WriterResult>builder()
            .record(record)
            .knownError(writeResult).build();
        }).collect(Collectors.toList());
        writeError.addAll(writeErrorRecords);
      }
      if (writeResult.isCommit()) {
        try {
          //防止commit异常
          dataSource.commit(tRecordConventSucces, settingContext);
        } catch (Exception e) {
          throw new ResetException("commit异常", e);
        }
      } else {
        tRecordConventSucces.forEach(record -> {
          rollbackUnit.put(dataSource.rollbackTransactionUnit(record), true);
        });
        dataSource.rollback(tRecordConventSucces, settingContext);
      }
    } catch (Exception e) {
      log.error("批量写入异常！", e);
      if (!(e instanceof ResetException)) {
        dataSource.commit(tRecordConventSucces, settingContext);
      } else {
        //这里认为回滚不会有异常情况
        tRecordConventSucces.forEach(record -> {
          rollbackUnit.put(dataSource.rollbackTransactionUnit(record), true);
        });
        dataSource.rollback(tRecordConventSucces, settingContext);
      }
      List<ErrorRecord<R, IDataSink.WriterResult>> writeErrorRecords = dataConventSuccess.stream().map(record -> {
        return ErrorRecord.<R, IDataSink.WriterResult>builder()
          .record(record)
          .unknownException(e).build();
      }).collect(Collectors.toList());
      writeError.addAll(writeErrorRecords);
    }
  }

  private void singleRecordWrite(IDataSource<T, S> dataSource, IDataSink<R, S> sink, List<R> writeSuccess, List<ErrorRecord<R, IDataSink.WriterResult>> writeError, T tRecord, List<R> records) {
    //单条写入
    try {

      IDataSink.WriterResult writerResult;
      if (records.size() == 1) {
        writerResult = sink.write(records.get(0), settingContext);
      } else {
        writerResult = sink.write(records, settingContext);
      }
      if (writerResult.isSuccess()) {
        writeSuccess.addAll(records);
      } else {
        for (R record : records) {
          writeError.add(ErrorRecord.<R, IDataSink.WriterResult>builder()
            .record(record)
            .knownError(writerResult).build());
        }
      }
      if (writerResult.isCommit()) {
        //数据提交
        try {
          //防止commit异常
          if (!settingContext.isBatchCommit())
            dataSource.commit(tRecord, settingContext);
        } catch (Exception e) {
          throw new ResetException("commit异常", e);
        }
      } else {
        //数据回滚
        //这里认为回滚不会有异常情况
        rollbackUnit.put(dataSource.rollbackTransactionUnit(tRecord), true);
        dataSource.rollback(tRecord, settingContext);
      }

    } catch (Exception e) {
      log.error("单条写入异常", e);
      if (!(e instanceof ResetException)) {
        //提交数据
        if (!settingContext.isBatchCommit())
          dataSource.commit(tRecord, settingContext);
      } else {
        //回滚数据
        rollbackUnit.put(dataSource.rollbackTransactionUnit(tRecord), true);
        dataSource.rollback(tRecord, settingContext);
      }
      for (R record : records) {
        writeError.add(ErrorRecord.<R, IDataSink.WriterResult>builder()
          .record(record)
          .unknownException(e).build());
      }
    }
  }

  private List<R> dataConvent(IDataConvertor<P, R, S> dataConvertor, List<R> dataConventSuccess, List<ErrorRecord<P, Object>> dataConventError, P protocalData) {
    try {
      Object record = dataConvertor.convert(protocalData, settingContext);
      if (record instanceof List) {
        dataConventSuccess.addAll((List) record);
        return (List) record;
      }
      dataConventSuccess.add((R) record);
      return Collections.singletonList((R) record);
    } catch (Exception e) {
      log.error("数据转换异常！", e);
      dataConventError.add(ErrorRecord.<P, Object>builder()
        .record(protocalData)
        .unknownException(e)
        .build());
    }
    return null;
  }

  private AbstractDataChecker.CheckResult protocalDataCheck(IProtocalDataChecker checker, List<P> checkerSuccess, List<ErrorRecord<P, AbstractDataChecker.CheckResult>> checkerError, P protocalData) {
    try {
      AbstractDataChecker.CheckResult checkResult = checker.check(null, protocalData, settingContext);
      if (checkResult.isPass()) {
        checkerSuccess.add(protocalData);
      } else {
        checkerError.add(ErrorRecord.<P, AbstractDataChecker.CheckResult>builder()
          .record(protocalData)
          .knownError(checkResult)
          .build());
      }
      return checkResult;
    } catch (Exception e) {
      log.error("校验发生异常！", e);
      checkerError.add(ErrorRecord.<P, AbstractDataChecker.CheckResult>builder()
        .record(protocalData)
        .unknownException(e)
        .build());
    }
    return null;
  }

  private P protocalConvent(IProtocalDataConvertor<T, P, S> protocalDataConvertor, List<P> protocalConventSuccess, List<ErrorRecord<T, Object>> protocalConventError, T tRecord) {
    try {
      P protocalData = protocalDataConvertor.convert(tRecord, settingContext);
      protocalConventSuccess.add(protocalData);
      return protocalData;
    } catch (Exception e) {
      log.error("协议转换失败！", e);
      protocalConventError.add(ErrorRecord.<T, Object>builder()
        .record(tRecord)
        .unknownException(e)
        .build());
    }
    return null;
  }

  @Override
  public void close() throws IOException {
    run = false;
    taskmonitor.setRun(run);
    safeListenerRun(() -> executorListeners.stream().forEach(listener -> listener.onClose(this, dataPipline, writeCount.longValue(), run)));
    while (!end) {
      //等待工作线程结束
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        log.error("线程被打断！");
      }
    }
    dataPipline.close();
  }

  public void setTaskMonitorMBean(TaskExecutorMonitor taskmonitor) {
    this.taskmonitor = taskmonitor;
  }
}